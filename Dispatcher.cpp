#include "Dispatcher.h"

Dispatcher::Dispatcher(sInputData input){
	features = input;
	
	numberOfTransacts = systemTime = processedSections = unprocessedSections = 0;
	missingProbability = queueAverageFirstDrive = queueAverageSecondDrive = 0.0;
	queueMaxFirstDrive = queueMaxSecondDrive = firstDrQNum = secondDrQNum = 0;
	
	S[S_FIRST_DRIVE] = 0;
	S[S_SECOND_DRIVE] = 0;
	
	for (size_t i = 0; i < ACTIVE_EVENTS; i++){
		A[i][A_TRANSACT_NUMBER] = 0;
		A[i][A_SIGN_OF_EVENT] = 0;
	}
	
	A[X1][A_EVENT_NUMBER] = X1;
	A[X2][A_EVENT_NUMBER] = X2;
	A[X3][A_EVENT_NUMBER] = X3;
	A[X4][A_EVENT_NUMBER] = X4;
	
	A[X1][A_TIME_OF_EVENT_START] = 0;
	A[X2][A_TIME_OF_EVENT_START] = features.firstGenTime + PlusMinus(features.firstGenTimeError);
	A[X3][A_TIME_OF_EVENT_START] = features.secondGenTime + PlusMinus(features.secondGenTimeError);
	A[X4][A_TIME_OF_EVENT_START] = features.sectionGenTime;

	A[X1][A_ACTIVITY] = AKA1;
	A[X2][A_ACTIVITY] = AKA2;
	A[X3][A_ACTIVITY] = AKA3;
	A[X4][A_ACTIVITY] = AKA4;	
	
	B[E1][B_EVENT_NUMBER] = E1; 
	B[E1][B_CONDITION_NUMBER] = E1_COND;
	B[E1][B_SIGN_OF_SATISFACTION] = 0;
	B[E1][B_ACTIVITY] = AKB1;
}

void Dispatcher::startModeling(){
	time_start();
	while(true){
		do {
			int i0 = findAMinTime();
			if (A[i0][A_TIME_OF_EVENT_START] > features.modelingTime){
				std::cout<<"_________________________________________________"<<std::endl;
				std::cout<<"Затраченное машинное время: "<<time_stop()<<std::endl;
				calculateVariables();
				printResults();
				return;
			} else {
				doActivity(A[i0][A_ACTIVITY]);
				if (checkCondition(B[E1][B_CONDITION_NUMBER] == true)){
					B[E1][B_SIGN_OF_SATISFACTION] = 1;
				} else {
					B[E1][B_SIGN_OF_SATISFACTION] = 0;
				}
			}
		} while (amountOfBSigns() == 0);
		doActivity(B[E1][B_ACTIVITY]);		
		B[E1][B_SIGN_OF_SATISFACTION] = 0;
	}	
}

void Dispatcher::doActivity(size_t activityNumber){
	switch (activityNumber){
		case AKA1: {
			A[X1][A_SIGN_OF_EVENT] = 1;
			systemTime = A[X1][A_TIME_OF_EVENT_START];
			A[X1][A_TIME_OF_EVENT_START] = systemTime + features.sectionGenTime;
			break;
		}
		case AKA2: {
			S[S_FIRST_DRIVE] += features.numOfTransFirstGen;
			if (S[S_FIRST_DRIVE] > queueMaxFirstDrive){
				queueMaxFirstDrive = S[S_FIRST_DRIVE];
			}
			firstDriveQueue[firstDrQNum] = S[S_FIRST_DRIVE];
			firstDrQNum++;
			generateTRtransacts(features.numOfTransFirstGen, TR_TYPE_FIRST);
			systemTime = A[X2][A_TIME_OF_EVENT_START];
			A[X2][A_TIME_OF_EVENT_START] = systemTime + features.firstGenTime + PlusMinus(features.firstGenTimeError);
			break;
		}
		case AKA3: {
			S[S_SECOND_DRIVE] += features.numOfTransSecondGen;
			if (S[S_SECOND_DRIVE] > queueMaxSecondDrive){
				queueMaxSecondDrive = S[S_SECOND_DRIVE];
			}		
			secondDriveQueue[secondDrQNum] = S[S_SECOND_DRIVE];
			secondDrQNum++;
			generateTRtransacts(features.numOfTransSecondGen, TR_TYPE_SECOND);
			systemTime = A[X3][A_TIME_OF_EVENT_START];
			A[X3][A_TIME_OF_EVENT_START] = systemTime + features.secondGenTime + PlusMinus(features.secondGenTimeError);			
			break;
		}
		case AKA4: {
			A[X1][A_SIGN_OF_EVENT] = 0;
			if (findProcessingTransacts() == true){
				processedSections++;
			} else {
				unprocessedSections++;
			}
			systemTime = A[X4][A_TIME_OF_EVENT_START];
			A[X4][A_TIME_OF_EVENT_START] = systemTime + features.sectionGenTime;			
			break;
		}
		case AKB1: {
			S[S_FIRST_DRIVE] -= features.firstDriveCapacity;
			S[S_SECOND_DRIVE] -= features.secondDriveCapacity;
			addTRsignOfProcessing();
			B[E1][B_SIGN_OF_SATISFACTION] = 0;
			break;
		}		
	}
}

void Dispatcher::calculateVariables(){
	float us = unprocessedSections;
	missingProbability = us/(processedSections+unprocessedSections);
	float amount;
	for (int i = 0; i < firstDrQNum; i++){
		amount += firstDriveQueue[i];
	}
	queueAverageFirstDrive = amount / firstDrQNum;
	amount = 0.0;
	for (int i = 0; i < secondDrQNum; i++){
		amount += secondDriveQueue[i];
	}
	queueAverageSecondDrive = amount / firstDrQNum;	
}

void Dispatcher::printResults(){	
	std::cout<<"_________________________________________________"<<std::endl;
	std::cout<<"Всего транзактов:"<<numberOfTransacts<<std::endl;
	std::cout<<"Транзактов обслужено:"<<numberOfServed()<<std::endl;
	std::cout<<"Транзактов не обслужено:"<<numberOfNotServed()<<std::endl;
	std::cout<<"Пустые секции:"<<unprocessedSections<<std::endl;
	std::cout<<"неПустые секции:"<<processedSections<<std::endl;
	std::cout<<"Возможность пропуска секции:"<<missingProbability*100<<"%"<<std::endl;
	std::cout<<"Средняя длина очереди в первом накопителе:"<<queueAverageFirstDrive<<std::endl;
	std::cout<<"Максимальная длина очереди в первом накопителе:"<<queueMaxFirstDrive<<std::endl;
	std::cout<<"Средняя длина очереди во втором накопителе:"<<queueAverageSecondDrive<<std::endl;
	std::cout<<"Максимальная длина очереди во втором накопителе:"<<queueMaxSecondDrive<<std::endl;
	std::cout<<"S[1]:"<<S[S_FIRST_DRIVE]<<std::endl;
	std::cout<<"S[2]:"<<S[S_SECOND_DRIVE]<<std::endl;
	printTR();
}

int Dispatcher::PlusMinus(int x){
	srand(time(NULL));
	return rand() % (x*2+1)-x;
}

bool Dispatcher::checkCondition(size_t conditionNumber){
	switch (conditionNumber){
		case E1_COND: {
			if ((A[X1][A_SIGN_OF_EVENT] == 1) && (S[S_FIRST_DRIVE] >= 10) && (S[S_SECOND_DRIVE] >= 10)){
				return true;
			} else {
				return false;
			}
		}
	}
}

size_t Dispatcher::amountOfBSigns(){
	return B[E1][B_SIGN_OF_SATISFACTION];
}

int Dispatcher::findAMinTime(){
	int min = SEARCH_INIT_MIN, i0;
	for (int i = 0; i < ACTIVE_EVENTS; i++){
		if (A[i][A_TIME_OF_EVENT_START] < min){
			min = A[i][A_TIME_OF_EVENT_START];
			i0 = i;
		}
	}
	return i0;
}

void Dispatcher::generateTRtransacts(size_t number, size_t type){
	for (int i = 0; i < number; i++){
		TR[numberOfTransacts][TR_TRANSACT_NUMBER] = numberOfTransacts;
		TR[numberOfTransacts][TR_GENERATION_TIME] = systemTime;
		TR[numberOfTransacts][TR_TYPE] = type;
		TR[numberOfTransacts][TR_SIGN_OF_PROCESSED] = 0;
		numberOfTransacts++;
	}
}

bool Dispatcher::findProcessingTransacts(){
	int f = 0;
	for (int i = 0; i < numberOfTransacts; i++){
		if (TR[i][TR_SIGN_OF_PROCESSING] == 1){
			f++;
			TR[i][TR_SIGN_OF_PROCESSING] = 0;
			TR[i][TR_SIGN_OF_PROCESSED] = 1; 
			if(f == features.firstDriveCapacity + features.secondDriveCapacity){
				return true;
			}			
		} 
	}
	return false;
}

void Dispatcher::addTRsignOfProcessing(){
	int f = 0, min = SEARCH_INIT_MIN;
	for (int i = 0; i < numberOfTransacts; i++){
		if ((TR[i][TR_TYPE] == TR_TYPE_FIRST) && (TR[i][TR_GENERATION_TIME] < min) && (f < features.firstDriveCapacity) 
		&& (TR[i][TR_SIGN_OF_PROCESSED] == 0)){
			TR[i][TR_SIGN_OF_PROCESSING] = 1;
			TR[i][TR_START_OF_SERVICE] = systemTime;
			f++; 
		}
	} 
	f = 0;
	min = SEARCH_INIT_MIN;
	for (int i = 0; i < numberOfTransacts; i++){
		if ((TR[i][TR_TYPE] == TR_TYPE_SECOND) && (TR[i][TR_GENERATION_TIME] < min) && (f < features.secondDriveCapacity) 
		&& (TR[i][TR_SIGN_OF_PROCESSED] == 0)){
			TR[i][TR_SIGN_OF_PROCESSING] = 1;
			TR[i][TR_START_OF_SERVICE] = systemTime;
			f++; 
		}			
	}
}

void Dispatcher::printTR(){
	std::ofstream out("TR.out");
	if (!out){
		std::cout<<"Ошибка при открытии файла на запись!"<<std::endl;
	}
	for (int i = 0; i < numberOfTransacts; i++){
		for (int j = TR_TRANSACT_NUMBER; j <= TR_TYPE; j++){
			if (j != TR_SIGN_OF_PROCESSING)
				out<<TR[i][j]<<" ";
		}
		out<<std::endl;
	}
}

int Dispatcher::numberOfServed(){
	int n = 0;
	for (int i = 0; i < numberOfTransacts; i++){
		if (TR[i][TR_SIGN_OF_PROCESSED] == 1){
			n++;
		}
	}
	return n;
}

int Dispatcher::numberOfNotServed(){
	int n = 0;
	for (int i = 0; i < numberOfTransacts; i++){
		if (TR[i][TR_SIGN_OF_PROCESSED] == 0){
			n++;
		}
	}
	return n;
}

void Dispatcher::time_start(){
	gettimeofday(&tv1, &tz); 
}

long Dispatcher::time_stop(){
	gettimeofday(&tv2, &tz);
	dtv.tv_sec= tv2.tv_sec - tv1.tv_sec;
	dtv.tv_usec=tv2.tv_usec - tv1.tv_usec;
	if(dtv.tv_usec < 0){
		dtv.tv_sec--;
		dtv.tv_usec+=1000000;
	}
	return dtv.tv_usec;
}
