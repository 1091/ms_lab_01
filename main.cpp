#include "DataInput.h"
#include "Dispatcher.h"

int main(){
	cDataInput input;
	input.startFill();
	Dispatcher dispatcher(input.getData());
	dispatcher.startModeling();
	return 0;
}

