#include "DataInput.h"

void cDataInput::startFill(){
	char ch;
	std::cout<<"Ввод исходных данных:"<<std::endl;
	std::cout<<"Данные по умолчанию/ввод других данных?\nДля ввода данных по умолчанию введите \'y\'"<<std::endl;
	std::cin>>ch;
	if (ch == 'y'){
		std::cout<<"Время моделирования (в минутах):" << input.modelingTime << std::endl;
		std::cout<<"Емкость накопителя для деталей первого типа:" << input.firstDriveCapacity << std::endl; //переформулировать
		std::cout<<"Емкость накопителя для деталей второго типа:" << input.secondDriveCapacity << std::endl; //переформулировать
		std::cout<<"Период генератора деталей первого типа:" << input.firstGenTime << std::endl;
		std::cout<<"Погрешность периода генератора деталей первого типа:" << input.firstGenTimeError << std::endl;
		std::cout<<"Период генератора деталей второго типа:"<< input.secondGenTime << std::endl;
		std::cout<<"Погрешность периода генератора деталей второго типа:" << input.secondGenTimeError << std::endl;
		std::cout<<"Период генератора секций конвейера:" << input.sectionGenTime << std::endl;
		std::cout<<"Число генерируемых деталей первого типа:" << input.numOfTransFirstGen << std::endl;
		std::cout<<"Число генерируемых деталей второго типа:" << input.numOfTransSecondGen << std::endl;
	} else {
		std::cout<<"Введите время моделирования (в минутах):"<<std::endl;
		std::cin>>input.modelingTime;
		std::cout<<"Введите емкость накопителя для деталей первого типа:"<<std::endl;
		std::cin>>input.firstDriveCapacity;
		std::cout<<"Введите емкость накопителя для деталей второго типа:"<<std::endl;
		std::cin>>input.secondDriveCapacity;
		std::cout<<"Введите период генератора деталей первого типа:"<<std::endl;
		std::cin>>input.firstGenTime;
		std::cout<<"Введите погрешность периода генератора деталей первого типа:"<<std::endl;
		std::cin>>input.firstGenTimeError;		
		std::cout<<"Введите период генератора деталей второго типа:"<<std::endl;
		std::cin>>input.secondGenTime;
		std::cout<<"Введите погрешность периода генератора деталей второго типа:"<<std::endl;
		std::cin>>input.secondGenTimeError;
		std::cout<<"Введите период генератора секций конвейера (такт конвейера):"<<std::endl;
		std::cin>>input.sectionGenTime;
		std::cout<<"Введите число генерируемых деталей первого типа:"<<std::endl; 
		std::cin>>input.numOfTransFirstGen;		
		std::cout<<"Введите число генерируемых деталей второго типа:"<<std::endl; 
		std::cin>>input.numOfTransSecondGen;
	}
}

sInputData cDataInput::getData(){
	return input;
}
